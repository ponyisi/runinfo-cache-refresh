from typing import Optional

def run_cache_refresh(livefile: Optional[str]):
    import time
    from pathlib import Path
    import xmlrpc.runinfodb_update
    countdown = 4
    while countdown > 0:
        try:
            xmlrpc.runinfodb_update.update()
            if livefile:
                Path(livefile).touch()
        except Exception as e:
            import traceback
            print('Unable to update run DB, reason:', e)
            print((traceback.format_exc()))
            # die if this keeps happening
            print('Number of retries allowed:', countdown)
            countdown -= 1
        time.sleep(120)

if __name__ == '__main__':
    from argparse import ArgumentParser
    parser = ArgumentParser()
    parser.add_argument('--livefile', help='File which will be touched on every pass')
    args = parser.parse_args()
    if args.livefile:
        print('Running with livefile:', args.livefile)
    run_cache_refresh(args.livefile)
