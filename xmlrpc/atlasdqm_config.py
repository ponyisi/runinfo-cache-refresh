from . import server_name

WEBDIRS = {
    'tier0':   '/dqmdisk0/www/tier0/Cosmics08',
    'reproc':  '/dqmdisk0/www/tier0/Cosmics08_r2',
    'online':  '/dqmdisk0/www/tier0/Cosmics08_online',
    'trigger': '/afs/cern.ch/atlas/project/trigger/trigcaf/www',
    'test':    '/eos/user/a/atlasdqm/www/test',
    'larcomm': '/afs/cern.ch/user/a/atlasdqm/dqmdisk/www/larcomm',
    'eos':     '/eos/atlas/atlascerngroupdisk/data-dqm/www/tier0/collisions',
    'larcommeos': '/eos/atlas/atlascerngroupdisk/data-dqm/www/larcomm',
}

HANDIRS = {
    'tier0':   '/dqmdisk0/han_results/tier0/Cosmics08',
    'reproc':  '/dqmdisk0/han_results/tier0/Cosmics08_r2',
    'online':  '/dqmdisk0/han_results/tier0/Cosmics08_online',
    'trigger': '/afs/cern.ch/atlas/project/trigger/trigcaf/han_results',
    'test':    '/eos/user/a/atlasdqm/han_results/test',
    'larcomm': '/afs/cern.ch/user/a/atlasdqm/dqmdisk/han_results/larcomm',
    'eos': '/eos/atlas/atlascerngroupdisk/data-dqm/han_results/tier0/collisions',
    'larcommeos': '/eos/atlas/atlascerngroupdisk/data-dqm/han_results/larcomm'
}

HTTPDIRS = {
    'tier0':   '/webdisplay/tier0',
    'reproc':  '/webdisplay/reproc',
    'online':  '/webdisplay/online',
    'trigger': '/webdisplay/trigger',
    'test':    '/webdisplay/test',
    'larcomm': '/webdisplay/larcomm',
    'eos': '/webdisplay/eos',
    'larcommeos': '/webdisplay/larcommeos'
}

DESCRIPTIONS = {
    'tier0':   'Tier 0 Processing',
    'reproc':  'Tier 1 Reprocessing',
    'online':  'Archived Online Histograms',
    'trigger': 'Trigger Reprocessing',
    'test':    'Test/Development',
    'larcomm': 'LAr Cleaned',
    'eos': 'eos test',
    'larcommeos': 'LAr Cleaned (EOS)',
}

COOLDB = "oracle://ATLAS_COOLPROD;schema=ATLAS_COOLOFL_GLOBAL;dbname=CONDBR2;"
VFDB = "oracle://ATLAS_COOLPROD;schema=ATLAS_COOLOFL_GLOBAL;dbname=CONDBR2;"
COOLFOLDER = "/GLOBAL/DETSTATUS"

RUNINFODB = "oracle://ATLAS_COOLPROD;schema=ATLAS_COOLONL_TDAQ;dbname=CONDBR2;"
TRIGDB = "oracle://ATLAS_COOLPROD;schema=ATLAS_COOLONL_TRIGGER;dbname=CONDBR2;"
DCSDB = "oracle://ATLAS_COOLPROD;schema=ATLAS_COOLOFL_DCS;dbname=CONDBR2;"

DEFECTSDB = { 'Test': "/afs/cern.ch/user/a/atlasdqm/dqmdisk1/cherrypy-devel/defectstest.db/COMP200",
              'Production': 'oracle://ATLAS_COOLWRITE;schema=ATLAS_COOLOFL_GLOBAL;dbname=CONDBR2',
              'Run 1': 'oracle://ATLAS_COOLWRITE;schema=ATLAS_COOLOFL_GLOBAL;dbname=COMP200' }

DEFECTSDB_RO = { 'Test': "/afs/cern.ch/user/a/atlasdqm/dqmdisk1/cherrypy-devel/defectstest.db/COMP200",
                 'Production': 'oracle://ATLAS_COOLPROD;schema=ATLAS_COOLOFL_GLOBAL;dbname=CONDBR2',
                 'Run 1': 'oracle://ATLAS_COOLPROD;schema=ATLAS_COOLOFL_GLOBAL;dbname=COMP200' }

DEFECTSDB_ORDER = [ 'Production', 'Run 1', 'Test' ]

COOL_CONNS = {
    'DQCALCOFL': "oracle://ATLAS_COOLPROD;schema=ATLAS_COOLOFL_GLOBAL;dbname=COMP200;",
    'DQMFOFL':   "oracle://ATLAS_COOLPROD;schema=ATLAS_COOLOFL_GLOBAL;dbname=COMP200;",
    'SHIFTOFL':  "oracle://ATLAS_COOLPROD;schema=ATLAS_COOLOFL_GLOBAL;dbname=COMP200;",
    'DCSOFL':    "oracle://ATLAS_COOLPROD;schema=ATLAS_COOLOFL_GLOBAL;dbname=COMP200;",
    'LBSUMM':    "oracle://ATLAS_COOLPROD;schema=ATLAS_COOLOFL_GLOBAL;dbname=COMP200;",
    'DQMFONL':   "oracle://ATLAS_COOLPROD;schema=ATLAS_COOLONL_GLOBAL;dbname=COMP200;",
    'DQMFONLLB': "oracle://ATLAS_COOLPROD;schema=ATLAS_COOLONL_GLOBAL;dbname=COMP200;",
    'SHIFTONL':  "oracle://ATLAS_COOLPROD;schema=ATLAS_COOLONL_GLOBAL;dbname=COMP200;",
    'DEFECTS':   "oracle://ATLAS_COOLPROD;schema=ATLAS_COOLOFL_GLOBAL;dbname=CONDBR2;",
}

EXTSERVER = server_name.SERVERNAME

#RUNINFOCACHE = '/afs/cern.ch/user/a/atlasdqm/dqmdisk1/cherrypy/runinfo.db'
#RUNINFOCACHE = 'sqlite:////dqmdisk0/runinfodb/runinfo.db'
RUNINFOCACHE = 'oracle://ATLAS_CONFIG/ATLAS_DQ_RESULTS'

ATLAS_RUNNUMBER_DB = 'oracle://ATLAS_COOLPROD/ATLAS_COOLONL_GLOBAL'
ATLAS_SFO_DB = 'oracle://ATLAS_CONFIG/ATLAS_SFO_T0'

FLAGCACHE = '/afs/cern.ch/user/a/atlasdqm/dqmdisk1/cherrypy/flags.db'

OLDINDEXES = {
    'tier0':   'results_Cosmics08.html',
    'reproc':  'results_Cosmics08_r2.html',
    'online':  'results.html',
    'test':    'results_TEST.html',
}

#RESULTCACHE = 'oracle://INTR/ATLAS_DQ_RESULTS'
RESULTCACHE = 'sqlite'
TIMESTAMPCACHE = 'oracle://ATLAS_CONFIG/ATLAS_DQ_RESULTS'
#TIMESTAMPCACHE = 'sqlite:////dqmdisk0/runinfodb/timestamp.db'

EXCLUDE_FROM_HANCACHE = ['online', 'test', 'trigger', 'larcomm', 'larcommeos']

SVN_REPLACE_STRINGS = ('svn+ssh://svn.cern.ch/reps/atlasoff', 'https://svnweb.cern.ch/trac/atlasoff/browser')

DEFECT_PASSWORD_FILE = '/afs/cern.ch/user/a/atlasdqm/private/defect_passwords.txt'

AMI_CONF = '/home/atlasdqm/private/AMIConf.txt'
XMLRPC_CONF = '/home/atlasdqm/private/dqauth.txt'
try:
    with open(XMLRPC_CONF) as f:
        XMLRPC_AUTH = f.read().strip()
except IOError:
    XMLRPC_AUTH = ''
XMLRPCSERVER = 'https://%s@%s' % (XMLRPC_AUTH, server_name.SERVERNAME)

SERVER_URL=server_name.SERVERNAME
WEBSERVER_URL='atlasdqm.web.cern.ch'

MOTD_DIRECTORY='/afs/cern.ch/user/a/atlasdqm/dqmdisk1/motd/'

AUTODEFECTSFILE='xmlrpc/auto_upload_defects.yaml'

NEMO_STATUS_FILE='/afs/cern.ch/user/a/atlcond/scratch0/nemo/prod/web/live_conditions.json'
