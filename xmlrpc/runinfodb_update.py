from sqlalchemy import (create_engine, MetaData, select, func, and_, Table)
from xmlrpc.dbauth import get_authentication
from DataQualityUtils.panic import panic

import os, decimal, datetime

from . import runinfodb_create, cool_extractor
from .atlasdqm_config import RUNINFOCACHE, ATLAS_RUNNUMBER_DB, ATLAS_SFO_DB
from .atlasdqm_config import DEFECTSDB, AUTODEFECTSFILE

AUTHFILE='/home/atlasdqm/private/param2.txt'

def _clamp(v, lo, hi):
    if v < lo:
        return lo
    elif v > hi:
        return hi
    else:
        return v

def fixup_magcurrents(magtup):
    return [_clamp(x, -1e6, 1e6) for x in magtup]

def fixup_lumi(beamtup):
    rv = beamtup[:]
    for i in range(3,5):
        rv[i] = _clamp(rv[i], 0, 1e6)

def get_physics_events(runlist):
    engine = create_engine(str(get_authentication(ATLAS_SFO_DB)))
    md = MetaData(engine)
    sfo_tz_overlap = Table('sfo_tz_overlap', md, autoload=True,
                           schema='ATLAS_SFO_T0')
    query = (select([sfo_tz_overlap.c.runnr,
                     func.sum(sfo_tz_overlap.c.overlap_events)],
                    and_(#sfo_tz_overlap.c.runnr.in_(runlist),
                         sfo_tz_overlap.c.type == 'EVENTCOUNT',
                         sfo_tz_overlap.c.reference_stream == 'physics_EventCount')
                    )
             .group_by(sfo_tz_overlap.c.runnr))
    results = engine.execute(query)
    rv = {}
    for row in results:
        if row[0] not in runlist: continue
        rv[row[0]] = int(row[1])
    return rv

def mark_unchecked_defects(runs, dbstr):
    print('here')
    import deprecated_defects
    from DQDefects import DefectsDB
    ddb = DefectsDB(dbstr, read_only=False)
    # UNCHECKED DEFECTS
    defects = [i for i in ddb.defect_names if '_UNCHECKED' in i 
               and i not in deprecated_defects.DEPRECATED_DEFECTS]
    # ADDITIONAL SPECIFIC DEFECTS - now moved to mark_defect_special_cases
    #defects.append('TILE_TIMEJUMPS_UNDEFINED')          
    with ddb.storage_buffer:
        for run, endlb, ptag in runs:
            iovs = ddb.retrieve(since = (run << 32 | 1),
                                until = (run << 32 | (endlb + 1)),
                                channels = defects,
                                nonpresent = True)
            existing_defects = [iov.channel for iov in iovs]
            for defect in defects:
                if ptag.endswith('_hi') or ptag.endswith('_hip'):
                    if defect in deprecated_defects.DEFECTS_NOT_FOR_HI:
                        print('defect', defect, 'not to be set for ptag', ptag)
                        continue
                if defect in existing_defects:
                    print('defect', defect, 'already in', existing_defects)
                    continue
                print('Inserting unchecked defect', defect, 'run', run)
                ddb.insert(defect_id = defect, since = (run << 32 | 1),
                           until = (run << 32 | (endlb + 1)),
                           comment = 'Automatically set',
                           added_by = 'sys:runinfodb_update',
                           recoverable = True)
                
def mark_defect_special_cases(runs, dbstr):
    # I really wish we didn't do this, but oh well - PUEO
    import deprecated_defects, yaml
    from DQDefects import DefectsDB
    ddb = DefectsDB(dbstr, read_only=False)
    # SPECIFIC DEFECTS TO FILL ALL THE TIME
    dlist = {}
    try:
        dlist = yaml.load(open(AUTODEFECTSFILE))
    except Exception as e:
        print('Unable to open and load', AUTODEFECTSFILE)
        print(e)
        #defects = [('TILE_TIMEJUMPS_UNDEFINED', 'Automatically set', 'sys:runinfodb_update', True),
               #('MS_CSC_EC_DISCONNECTED_2', 'Automatically set - permanently disconnected chambers', 'sys:runinfodb_update', False),
               #('MBTS_LOWHV', 'Automatically set - both sides of MBTS with non nominal HV', 'sys:runinfodb_update', False)
        #]
    with ddb.storage_buffer:
        defects = [_['name'] for _ in dlist if 'name' in _]
        for run, endlb, ptag in runs:
            iovs = ddb.retrieve(since = (run << 32 | 1),
                                until = (run << 32 | (endlb + 1)),
                                channels = defects,
                                nonpresent = True)
            existing_defects = [iov.channel for iov in iovs]
            #for defect, comment, added_by, recoverable in defects:
            for defectinfo in dlist:
                if 'name' not in defectinfo: continue
                defect = defectinfo['name']
                comment = defectinfo.get('comment', 'Automatically set')
                added_by = defectinfo.get('added_by', 'sys:runinfodb_update')
                present = defectinfo.get('present', True)
                recoverable = defectinfo.get('recoverable', False)
                override = defectinfo.get('override', False)
                if not isinstance(present, bool): present = True
                if not isinstance(recoverable, bool): recoverable = False
                if not isinstance(override, bool): override = False
                if ptag.endswith('_hi') or ptag.endswith('_hip'):
                    if defect in deprecated_defects.DEFECTS_NOT_FOR_HI:
                        print('defect', defect, 'not to be set for ptag', ptag)
                        continue
                if defect in existing_defects and not override:
                    print('defect', defect, 'already in', existing_defects)
                    continue
                print('Inserting special defect', defect, 'run', run)
                ddb.insert(defect_id = defect, since = (run << 32 | 1),
                           until = (run << 32 | (endlb + 1)),
                           comment = comment,
                           added_by = added_by,
                           present = present,
                           recoverable = recoverable)
            #if [iov for iov in iovs if iov.channel == 'ZDC_DISABLED' and iov.present]:
            #    # ZDC disabled, do not bother shifters; force unchecked off
            #    print 'Unsetting ZDC_UNCHECKED run', run
            #    ddb.insert(defect_id = 'ZDC_UNCHECKED', since = (run << 32 | 1),
            #               until = (run << 32 | (endlb + 1)),
            #               present = False,
            #               comment = 'Automatically set',
            #               added_by = 'sys:runinfodb_update',
            #               recoverable = False)
                
def error(info, url):
    panic('%s\n'
          'Fix source of issue (possibly logbook/Django is not running?) and access the following URL:\n'
          '%s\n' 
          'NOTE: You will almost certainly get more than one of these emails.  Be sure to access the URLs in the order which you got them.  Do not be surprised if you get no visible output.' % (info, url.replace('http://localhost:8081', 'https://atlasdqm.cern.ch')))

def add_run_to_elisa(run, start, end):
    print('Adding to ELisA')
    from elisa_client_api.elisa import Elisa
    from elisa_client_api.messageInsert import MessageInsert
    from elisa_client_api.messageReply import MessageReply
    from elisa_client_api.optionsBuilder import OptionsBuilder
    import time
    pwinfo = list(open(AUTHFILE, 'r').readlines())
    pwinfo = list(map(str.strip, pwinfo))

    el = Elisa('https://atlasdqlog.cern.ch/elisa/api/DQshift/', ssocookie=os.environ.get('SSO_COOKIE_FILE', '/home/atlasdqm/private/ssocookie_elisa.txt'))
    try:
        systems=el.getSystemsAffected()
    except Exception as e:
        # SSO login nonsense?
        print('Trapped exception', e)
        print('Will wait a minute and try again')
        import time; time.sleep(60)
        systems=el.getSystemsAffected()
    # insert head
    mi = MessageInsert()
    mi.author='DQM System'
    #mi.username='atlasdqm'
    mi.type='DQSummary'
    #mi.type='Default Message Type'
    #mi.RunNumber='%d' % run
    #mi.options=[{'name': 'RunNumber', 'value': '%d' % run, 'options': []}]
    mi.body='Run %d\nStart: %s\nEnd: %s' % (run,
                                            time.strftime('%Y-%m-%d %H:%H:%M %Z', time.localtime(start)),
                                            time.strftime('%Y-%m-%d %H:%H:%M %Z', time.localtime(end)))
    mi.systemsAffected=['Run']
    mi.subject='Run %d' % run
    ob = OptionsBuilder()
    taOpt = ob.addOption('RunNumber','%d'%run)
    mi.options = ob.toList()
    headmsg = el.insertMessage(mi)
    # insert Summary, ES1 & BLK
    mr = MessageReply(headmsg.id)
    mr.subject='Summary for Run %d' % run
    mr.body='Discuss overall issues affecting all systems here.'
    mr.author='DQM System'
    mr.systemsAffected=['Run']
    mr.options = ob.toList()
    el.replyToMessage(mr)
    mr.subject='ES1, Run %d (Tier-0 express reconstruction)' % run
    mr.body='Perhaps some more useful info here'
    mr.author='DQM System'
    mr.systemsAffected=['Processing_Pass']
    el.replyToMessage(mr)
    mr.subject='BLK, Run %d (Tier-0 express reconstruction)' % run
    el.replyToMessage(mr)
    

def add_run_to_signoff(run, start, end):
    import urllib.request, urllib.parse, urllib.error
    # define run
    data = { 'run': run, 'starttime': start, 'endtime': end }
    try:
        url='http://localhost:8081/dqsignoff/newrun/?%s' % urllib.parse.urlencode(data)
        f=urllib.request.urlopen(url)
        if f.getcode() != 200:
            print('Error creating run, error code', f.getcode())
            error('Error creating run %s on logbook, error code %s' % (run, f.getcode()), url)
            # Probably the run exists already
            return
        f.read()
    except IOError as e:
        print('Error opening URL;', e)
        error('Error creating run %s on logbook, error %s' % (run, e), url)
    # ES1
    data = { 'run': run, 'processing': 'ES1' }
    try:
        url='http://localhost:8081/dqsignoff/newrunprocessing/?%s' % urllib.parse.urlencode(data)
        f=urllib.request.urlopen(url)
        if f.getcode() != 200:
            print('Error creating ES1, error code', f.getcode())
            error('Error creating run %s on logbook, error code %s' % (run, f.getcode()), url)
        f.read()
    except IOError as e:
        print('Error opening URL;', e)
        error('Error creating run %s on logbook, error %s' % (run, e), url)
    data['processing'] = 'BLK'
    try:
        url='http://localhost:8081/dqsignoff/newrunprocessing/?%s' % urllib.parse.urlencode(data)
        f=urllib.request.urlopen(url)
        if f.getcode() != 200:
            print('Error creating BLK, error code', f.getcode())
            error('Error creating run %s on logbook, error code %s' % (run, f.getcode()), url)
        f.read()
    except IOError as e:
        print('Error opening URL;', e)    
        error('Error creating run %s on logbook, error %s' % (run, e), url)

def update(fname=RUNINFOCACHE):
    print('accessing', RUNINFOCACHE)
#    if not os.access(fname, os.R_OK):
    fullauth = str(get_authentication(fname))
    runinfodb_create.create(connstring=fullauth)

    #db = sqlite.connect(fname)
    engine = create_engine(fullauth, coerce_to_decimal=False)
    conn = engine.connect()
    md = MetaData(engine); md.reflect(schema='atlas_dq_results')
    runinfo = md.tables['atlas_dq_results.runinfo']
    #runinfo = Table('runinfo', md, schema='atlas_dq_results', autoload=True)

    ext_engine = create_engine(str(get_authentication(ATLAS_RUNNUMBER_DB)))
    run_table = Table('runnumber', MetaData(ext_engine), 
                      schema='ATLAS_RUN_NUMBER', autoload=True)

    max_run = conn.execute(select([func.max(runinfo.c.run)])).scalar()
    print('Max run:', max_run)
#    cur = db.cursor()
#    tcur = cur.execute('SELECT MAX(run) from runinfo')
#    buf = tcur.fetchone()
    if max_run is None:
        run_spec = {'low_run': 60000,
                    'high_run': 61000 }
    else:
        run_spec = {'low_run': max_run+1,
                    'high_run': max_run+30000}

    print('extract COOL info: run info,')
    ri = cool_extractor.get_run_information(run_spec)
    print(list(ri.keys()))
    print('mag fields')
    mag = cool_extractor.get_run_magfields(run_spec)
    print(list(mag.keys()))
    print('beam info')
    beam = cool_extractor.get_run_beamluminfo(run_spec)
    print(list(beam.keys()))
    print()
    print(datetime.datetime.now())
    print('Getting physics event counts')
    intruns = sorted(map(int, ri))
    physics_events = get_physics_events(intruns)
    print('begin transaction')
    #cur.execute('BEGIN TRANSACTION')
    transaction = conn.begin()
    ins = runinfo.insert()
    to_publish_to_mq = []
    for run in intruns:
        if repr(run) not in mag: continue
        rirow = ri[repr(run)]; magrow = fixup_magcurrents(mag[repr(run)])
        if repr(run) not in beam:
            beamrow = [0, False, False, 0, 0]
        else:
            beamrow = beam[repr(run)]
        print('NEW', run, rirow, magrow, beamrow)
        print(repr(beamrow))
#        cur.execute('''INSERT INTO runinfo(run, run_type, project_tag,
#                       partition,
#                       ef_events, run_start, run_end, lb, data_source,
#                       det_mask, rec_enable, sol_current, tor_current,
#                       sol_set_current, tor_set_current)
#                       VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)''',
#                    (int(run), rirow[0], rirow[1], rirow[2], rirow[3],
#                     rirow[4], rirow[5], int(rirow[6]), rirow[7],
#                     int(rirow[8]), rirow[9], magrow[0], magrow[1],
#                     magrow[2], magrow[3]))
        partition = rirow[2]
        if partition == '':
            partition = ext_engine.execute(select([run_table.c.partitionname], run_table.c.runnumber == run)).scalar()
            print('fixup partition name for run', run, 'to', partition)

        conn.execute(ins, run=run, run_type=rirow[0], 
                     project_tag=rirow[1], partition=partition,
                     ef_events=rirow[3], run_start=rirow[4],
                     run_end=rirow[5], lb=int(rirow[6]), data_source=rirow[7],
                     det_mask=decimal.Decimal(rirow[8]), 
                     rec_enable=int(rirow[9]),
                     sol_current=magrow[0], tor_current=min(magrow[1],1e38),
                     sol_set_current=magrow[2], tor_set_current=magrow[3],
                     physics_events=physics_events.get(run, -1),
                     max_energy=beamrow[0], has_stable_beam=int(beamrow[1]),
                     has_ready=int(beamrow[2]), lumi_total=beamrow[3],
                     lumi_ready=beamrow[4])
        to_publish_to_mq.append((run, {'run_type': rirow[0],
                                'project_tag': rirow[1],
                                'partition': partition,
                                'run_start': rirow[4],
                                'det_mask': rirow[8],
                                'rec_enable': bool(rirow[9])},
                                 False))
        #publish_run_to_mq(run, {'run_type': rirow[0],
        #                        'project_tag': rirow[1],
        #                        'partition': partition,
        #                        'run_start': rirow[4],
        #                        'det_mask': rirow[8],
        #                        'rec_enable': bool(rirow[9])},
        #                  False)

        if rirow[5] != 0 and int(rirow[6]) != 0 and partition == 'ATLAS':
            mark_unchecked_defects([(run, int(rirow[6]), rirow[1])], DEFECTSDB['Production'])
            mark_defect_special_cases([(run, int(rirow[6]), rirow[1])], DEFECTSDB['Production'])
            #add_run_to_signoff(run, rirow[4], rirow[5])
            add_run_to_elisa(run, rirow[4], rirow[5])

    #transaction.commit()
    #transaction = conn.begin()

    max_run = conn.execute(select([func.max(runinfo.c.run)])).scalar()
#    tcur = cur.execute('SELECT MAX(run) from runinfo')
#    buf = tcur.fetchone()
    if max_run is None:
        print("No runs: this shouldn't happen")
        max_run = 60000

    # now look up any EOR/beam updates in the last 1000 runs
    eorruns = list(conn.execute(select([runinfo.c.run],
                                       and_(runinfo.c.run >= max_run-1000,
                                            runinfo.c.run_end == 0))))
    eorruns.sort()
    
    try:
        physics_events = get_physics_events(list(map(int, list(zip(*eorruns))[0])))
    except:
        physics_events = {}
    #cur.execute('SELECT run FROM runinfo WHERE run >= ? AND run_end = 0', (max_run-2000,))
    upd = runinfo.update()
    for runrow in eorruns:
        run = runrow[0]
        run_spec = {'low_run': run, 'high_run': run}

        ri = cool_extractor.get_run_information(run_spec)
        rirow = ri[repr(run)]
        # update beam info
        beam = cool_extractor.get_run_beamluminfo(run_spec)
        beamrow = beam.get(repr(run), [0, False, False, 0, 0])
        print('update beam info for', run, beamrow)
        conn.execute(upd.where(runinfo.c.run == run)
                     .values(max_energy=beamrow[0], has_stable_beam=int(beamrow[1]),
                             has_ready=int(beamrow[2]), lumi_total=beamrow[3],
                             lumi_ready=beamrow[4])
                     )
        # no EOR yet
        if rirow[5] == 0: continue
        partition = rirow[2]
        if partition == '':
            partition = ext_engine.execute(select([run_table.c.partitionname], run_table.c.runnumber == run)).scalar()
            print('fixup partition name for run', run, 'to', partition)

        mag = cool_extractor.get_run_magfields(run_spec)
        if repr(run) not in mag: continue
        magrow = fixup_magcurrents(mag[repr(run)])
        print('EOR UPDATING', run, rirow, magrow)
        conn.execute(upd.where(runinfo.c.run == run)
                     .values(run_type=rirow[0], 
                             project_tag=rirow[1],
                             ef_events=rirow[3], run_start=rirow[4],
                             run_end=rirow[5], lb=int(rirow[6]), 
                             data_source=rirow[7],
                             det_mask=decimal.Decimal(rirow[8]), 
                             rec_enable=int(rirow[9]),
                             sol_current=magrow[0], 
                             tor_current=min(magrow[1],1e38),
                             sol_set_current=magrow[2], 
                             tor_set_current=magrow[3],
                             physics_events=physics_events.get(run,-1),
                             #max_energy=beamrow[0], has_stable_beam=beamrow[1],
                             #has_ready=beamrow[2], lumi_total=beamrow[3],
                             #lumi_ready=beamrow[4]
                             )
                     )
        to_publish_to_mq.append((run, {'run_type': rirow[0],
                                'project_tag': rirow[1],
                                'partition': partition,
                                'ef_events': rirow[3],
                                'run_start': rirow[4],
                                'run_end': rirow[5],
                                'lb': int(rirow[6]),
                                'det_mask': rirow[8],
                                'rec_enable': bool(rirow[9]),
                                'sol_current': magrow[0],
                                'tor_current': min(magrow[1],1e38),
                                'sol_set_current': magrow[2],
                                'tor_set_current': magrow[3],
                                'physics_events': physics_events.get(run,-1),
                                'has_stable_beam': beamrow[1]
                                },
                          True))
        #publish_run_to_mq(run, {'run_type': rirow[0],
        #                        'project_tag': rirow[1],
        #                        'partition': partition,
        #                        'ef_events': rirow[3],
        #                        'run_start': rirow[4],
        #                        'run_end': rirow[5],
        #                        'lb': int(rirow[6]),
        #                        'det_mask': rirow[8],
        #                        'rec_enable': bool(rirow[9]),
        #                        'sol_current': magrow[0],
        #                        'tor_current': min(magrow[1],1e38),
        #                        'sol_set_current': magrow[2],
        #                        'tor_set_current': magrow[3],
        #                        'physics_events': physics_events.get(run,-1),
        #                        'has_stable_beam': beamrow[1]
        #                        },
        #                  True)
        if int(rirow[6]) != 0 and partition == 'ATLAS':
            mark_unchecked_defects([(run, int(rirow[6]), rirow[1])], DEFECTSDB['Production'])
            mark_defect_special_cases([(run, int(rirow[6]), rirow[1])], DEFECTSDB['Production'])
            #add_run_to_signoff(run, rirow[4], rirow[5])
            add_run_to_elisa(run, rirow[4], rirow[5])

#        cur.execute('''UPDATE runinfo
#                       SET run_type = ?, project_tag = ?,
#                       partition = ?, ef_events = ?,
#                       run_start = ?, run_end = ?, lb = ?, data_source =?,
#                       det_mask = ?, rec_enable = ?, sol_current = ?,
#                       tor_current = ?, sol_set_current = ?,
#                       tor_set_current = ?
#                       WHERE run = ?
#                       ''',
#                    (rirow[0], rirow[1], rirow[2], rirow[3],
#                     rirow[4], rirow[5], int(rirow[6]), rirow[7],
#                     int(rirow[8]), rirow[9], magrow[0], magrow[1],
#                     magrow[2], magrow[3], run))

    #cur.execute('COMMIT TRANSACTION')
    print('End transaction')
    transaction.commit()
    conn.close()
    print('Publishing to MQ')
    for topublish in to_publish_to_mq:
        try:
            publish_run_to_mq(*topublish)
        except Exception as e:
            panic('Failed to publish run information to MQ.\n\nGeneral info:\n%s\n\nException: %s' % (topublish, e))
    print('done')
    #db.commit()
    #db.close()
    #del db

def get_connection():
    pass

def publish_run_to_mq(run, runinfo, isEOR):
    import stomp, random, messaging.message, json
    # from DataQualityUtils import stompconfig
    dest='/topic/atlas.dqm.progress'
    cfg = {'username': os.environ.get('MQ_USERNAME', 'atlasdqm'),
           'passcode': os.environ.get('MQ_PASSCODE', '')
          }
    if stomp.__version__ >= (6,1,0):
        # cfg = stompconfig.config()
        cfg['reconnect_attempts_max'] = 3
        cfg['version'] = 1.1
        conn=stomp.Connection([('atlas-mb.cern.ch', 61013)], heartbeats=(180000,180000))
        #conn.start()
        print('started connection')
        conn.connect(wait=True, **cfg)
        print('connected')
    else:
        # cfg=stompconfig.config()
        cfg['heartbeats'] = (0,0)
        cfg['reconnect_attempts_max'] = 3
        cfg['version'] = 1.1
        conn=stomp.Connection([('atlas-mb.cern.ch', 61013)], cfg)
        conn.start()
        print('started connection')    
        conn.connect(wait=True)
        print('connected')

    message = messaging.message.Message(header={'MsgClass':'DQ', 'MsgType': 'RunStart' if not isEOR else 'RunEnd', 'type':'textMessage', 'persistent': 'true',
                                                'destination': dest,
                                                'ack': 'auto',
                                                },
                                        body=json.dumps({'run': run, 'runinfo': runinfo}))
    #message.body=json.dumps({'run': run, 'runinfo': runinfo})
    print(message)
    conn.send(body=message.body, **message.header)
    print('sent message')
    conn.disconnect()

if __name__ == '__main__':
    update()
