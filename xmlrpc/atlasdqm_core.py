from .atlasdqm_config import *

class lowhighrange(object):
    def __init__(self, low, high):
        self.low = low
        self.high = high

    def __contains__(self, key):
        if self.low <= key < self.high and int(key) == key:
            return True
        else:
            return False

    def __iter__(self):
        return range(self.low, self.high).__iter__()

    def __getitem__(self, index):
        return self.low + index

    def __len__(self):
        return self.high-self.low if self.high > self.low else 0

def _canonicalize_runspec(run_spec):
    run_spec['proc_ver'] = int(run_spec.get('proc_ver', 1))
    if 'run_list' not in run_spec:
        run_spec['low_run'] = int(run_spec.get('low_run', 0))
        run_spec['high_run'] = int(run_spec.get('high_run', 0xffffffff))
        run_spec['run_list'] = lowhighrange(run_spec['low_run'],
                                      run_spec['high_run']+1)
    else:
        #run_spec['run_list'] = tuple(run_spec['run_list'])
        run_spec.pop('low_run', None)
        run_spec.pop('high_run', None)
        #run_spec['low_run'] = min(run_spec['run_list'])
        #run_spec['high_run'] = max(run_spec['run_list'])
        
    run_spec['stream'] = run_spec.get('stream', 'physics_IDCosmic')
    run_spec['source'] = run_spec.get('source', 'tier0')
    run_spec['period_type'] = run_spec.get('period_type', 'run')
    run_spec['period'] = int(run_spec.get('period', 1))

def get_next_proc_pass(run, stream, source):
    """
This function is intended for internal DQMF infrastructure use.

Usage: get_next_proc_pass(run, stream, source)

Arguments:

    * run: run as an integer
    * stream: stream as a string
    * source: DQMF source as a string; valid options are 'tier0' and 'reproc' 

Returns:

    * integer indicating the value that should be used as the next 'processing
      pass' variable. 
    """
    if source not in HANDIRS:
        raise ValueError('%s is not a known source of web display data'
                         % source)
    topdir = HANDIRS[source]
    import glob, os, re
    fmatches = glob.glob('%s/*/%s/run_%s' % (topdir, stream, run))
    ex = re.compile('%s\/([0-9]*?)/%s' % (topdir, stream))
    matches = [ex.match(i) for i in fmatches]
    matches = [int(i.group(1)) for i in matches if i != None and i.group(1) != None]
    if len(matches) == 0:
        return 1
    else:
        return max(matches) + 1
