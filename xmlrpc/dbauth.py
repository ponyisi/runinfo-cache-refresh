def get_authentication(connection="oracle://ATLAS_COOLPROD/ATLAS_COOLOFL_GLOBAL", user=None):
    """
    Retrieves authentication information from CORAL_AUTH_PATH authentication.xml
    """
    
    if connection[:6] == 'sqlite':
        return connection
    if '@' in connection:
        return connection

    from os import environ
    from os.path import join as pjoin
    import re
    assert "CORAL_AUTH_PATH" in environ, "CORAL_AUTH_PATH environment var not set"

    authentication = None
    from xml.dom.minidom import parse

    auth_path = environ["CORAL_AUTH_PATH"].split(":")[0]

    dom = parse(pjoin(auth_path, "authentication.xml"))

    connections = dom.getElementsByTagName("connection")
    if user is None:
        desired_conn = lambda c: c.attributes.get("name").value == connection
    else:
        def desired_conn(c):
            if c.attributes.get("name").value != connection: return False
            for _ in c.getElementsByTagName("parameter"):
                if (_.attributes.get("name").value == "user" 
                    and _.attributes.get("value").value == user):
                    return True
            return False

    connections = list(filter(desired_conn, connections))

    assert len(connections) >= 1, "Couldn't find connection string"

    info = {}
    for node in connections[0].childNodes:
        if node.nodeName == "parameter":
            info[node.getAttribute("name")] = node.getAttribute("value")

#            authentication = info["user"], info["password"]

    match = re.search('oracle://([^/]*)/', connection)
    if match is None:
        raise ValueError('apparently malformed connection string ' + connection)
    return str('oracle://%s:%s@%s' % (info['user'], info['password'], match.group(1)))
