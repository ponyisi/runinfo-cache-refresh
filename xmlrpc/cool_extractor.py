from __future__ import with_statement

from .atlasdqm_config import (COOLDB, COOLFOLDER, RUNINFODB, DCSDB, COOL_CONNS,
                             TRIGDB, VFDB, DEFECTSDB)
from . import atlasdqm_core

THIRTYTWOMASK = 2**32-1

# following map: channel number -> index of returned list for get_run_magfields
magfieldmap_ext = { 1: 0,
                    2: 2,
                    3: 1,
                    4: 3 }
magfieldmap = magfieldmap_ext

#from DataQualityUtils.DBInfo_Histo import *

#metadata = getChannelDict()

#invmetadata = {}
#for key, val in list(metadata.items()):
#    invmetadata[val] = key

codeMap = {
    -1: "Disabled",
    0: "Undefined",
    1: "Red",
    2: "Yellow",
    3: "Green"
    }

from PyCool import cool, coral
dbSvc = cool.DatabaseSvcFactory.databaseService()
#from VirtualFlags import VirtualFlagFolder, VirtualFlagLogicFolder

import contextlib

def getauth():
    return [x.strip() for x in open('/home/atlasdqm/private/param.txt').readlines()]

@contextlib.contextmanager
def _get_db(dbstring, readonly=True):
    db = None
    try:
        db = dbSvc.openDatabase(dbstring, readonly)
        yield db
    finally:
        if db is not None:
            db.closeDatabase()
        #from DQUtils.db import Databases
        #Databases.clear_connections()

def _fromIOV(iov):
    return (iov >> 32, iov & 2**32-1)

def get_dqmf_summary_flags(run_spec, flags, folder='DQMFOFL', tag='HEAD'):
    """
Usage: get_dqmf_summary_flags(run_spec, flags, folder, tag)

Arguments:

   * run_spec: run specification (see below)
   * flags: DQMF flags. This argument can be either a single flag 
     (e.g. 'PIXB') or a list of flags (e.g. ['PIXB', 'PIX0']).
   * folder: COOL folder (default: DQMFOFL)
   * tag: COOL tag (default: HEAD)

Returns:

   * dictionary with keys being run numbers (as strings) and values being:
      * if the "flags" argument is a string, the value will be the
        value of the DQMF check in LB 1 of that run;
      * if the "flags" argument is a list of strings, the values will be
        dictionaries with keys being the flag names and values being the
        value of the DQMF check in LB 1 of that run.
    """
    if type(flags) == str:
        flags = [flags]
        newfmt = False
    else:
        newfmt = True

    if len(flags) < 1:
        return {}

    flags = list(map(str.upper, flags))
    #for flag in flags:
    #    if flag not in metadata:
    #        raise ValueError('%s is not a known DQMF summary code' % flag)
    
    atlasdqm_core._canonicalize_runspec(run_spec)

    rv = {}
    with _get_db(COOL_CONNS[folder], True) as db:
        with _get_db(VFDB, True) as vfdb:
            #db = dbSvc.openDatabase()
            folder_in = folder
            vfl = VirtualFlagLogicFolder(vfdb)
            print('=================================================VFL============')
            folder = VirtualFlagFolder(db.getFolder(COOLFOLDER + '/' + folder),
                                       vfl)
            #tag = folder.resolveTag(tag) if tag != 'HEAD' else tag

            run_list = run_spec['run_list']
            if run_list == []:
                return rv
            lowrun = min(run_list)
            highrun = max(run_list)

            objects = folder.browseObjects((lowrun << 32) + 1,
                                           (highrun << 32) + 0xFFFFFFFF,
                                           flags,
                                           tag)

            for obj in objects:
                #if folder.channelName(obj.channelId()) not in flags:
        ##        if invmetadata.get(int(obj.channelId()), '') not in flags:
                #    continue
                sincerun = obj.since >> 32
                sincelb = obj.since & THIRTYTWOMASK
                untilrun = obj.until >> 32
                untillb = int(obj.until & THIRTYTWOMASK)
                if (sincerun != untilrun and 
                    (untilrun - sincerun != 1) and
                    (obj.until | THIRTYTWOMASK != 0)):
                    print('In get_dqmf_summary_flags:')
                    print('COOL flag over more than one run!', _fromIOV(obj.since), _fromIOV(obj.until), obj.channel)
                    #print invmetadata[obj.channelId()], obj.since >> 32, obj.since & (2**32-1), obj.until >> 32, obj.until & (2**32-1), codeMap[obj.payload()['Code']]
                # is this the flag at the start of run?
                if not (sincelb <= 1 < untillb):
                    continue
                if sincerun in run_list: 
                    if obj.Code not in codeMap: continue
                    runstr = repr(int(sincerun))
                    if newfmt:
                        #channel = invmetadata[int(obj.channelId())]
                        channel = folder.channelName(obj.channel)
                        if runstr not in rv:
                            rv[runstr] = {}
                        rv[runstr][channel] = codeMap[obj.Code]
                    else:
                        rv[runstr] = codeMap[obj.Code]

    #db.closeDatabase()
    del folder; del vfl

    return rv

def get_dqmf_summary_flags_lb(run_spec, flags, folder='DQMFOFL', tag='HEAD'):
    """
Usage: get_dqmf_summary_flags_lb(run_spec, flag, folder, tag)

Arguments:

    * run_spec: run specification (see below)
    * flags: DQMF flags.  This argument can be either a single flag 
      (e.g. 'PIXB') or a list of flags (e.g. ['PIXB', 'PIX0']).
    * folder: COOL folder (default: DQMFOFL)
    * tag: COOL tag (default: HEAD)

Returns:

    * dictionary with keys being run numbers (as strings) and values being:
        * if the "flags" argument is a string, the values will be a list of
          three-tuples (startlb, endlb+1, flag)
        * if the "flags" argument is a list of strings, the values will be
          dictionaries with keys being the flag names and values being a
          list of three-tuples (startlb, endlb+1, flag).
    """
    if type(flags) == str:
        flags = [flags]
        newfmt = False
    else:
        newfmt = True

    if len(flags) < 1:
        return {}
    
    flags = list(map(str.upper, flags))
#    for flag in flags:
#        if flag not in metadata:
#            raise ValueError('%s is not a known DQMF summary code' % flag)
    
    atlasdqm_core._canonicalize_runspec(run_spec)

    rv = {}
    with _get_db(COOL_CONNS[folder], True) as db:
        with _get_db(VFDB, True) as vfdb:
            vfl = VirtualFlagLogicFolder(vfdb)
            print('=================================================VFL============')
            folder_in = folder
            folder = VirtualFlagFolder(db.getFolder(COOLFOLDER + '/' + folder),
                                       vfl)
            #tag = folder.resolveTag(tag) if tag != 'HEAD' else tag

            run_list = run_spec['run_list']
            if run_list == []:
                return rv
            lowrun = min(run_list)
            highrun = max(run_list)

            channels = cool.ChannelSelection()
            #for flag in flags[1:]:
            #    channels.addChannel(metadata[flag])
            try:
                objects = folder.browseObjects((lowrun << 32) + 1,
                                               (highrun << 32) + THIRTYTWOMASK,
                                               flags,
                                               tag)
            except:
                print('EXCEPT', lowrun, highrun, flags, folder_in, tag, folder.folder, vfl.folder)
                raise
            #raise `objects`
            #raise `vfl.get_available_flags()`
            SIMAX=int(2**31-1)
            for obj in objects:
                #if invmetadata.get(int(obj.channelId()), '') not in flags:
                #if folder.channelName(obj.channel) not in flags:
                #    continue
                sincerun = obj.since >> 32
                sincelb = obj.since & THIRTYTWOMASK
                untilrun = obj.until >> 32
                untillb = int(obj.until & THIRTYTWOMASK)
                if (sincerun != untilrun and 
                    ((untilrun - sincerun != 1) or
                    (untillb != 0))):
                    print('In get_dqmf_summary_flags:')
                    print('COOL flag over more than one run!', _fromIOV(obj.since), _fromIOV(obj.until), obj.channel)
                    print(flag, folder_in, tag)
                    #print invmetadata[obj.channelId()], obj.since >> 32, obj.since & (2**32-1), obj.until >> 32, obj.until & (2**32-1), codeMap[obj.payload()['Code']]
                if untilrun - sincerun == 1:
                    if untillb == 0:
                        untilrun = sincerun
                        untillb = -1
                    elif sincelb == 0xFFFFFFFF and untillb <= 1:
                        # invalid info
                        continue
                    else:
                        print("COOL flag doesn't break down by run here: %s %s %s %s" % (sincerun, sincelb, untilrun, untillb))

                        #raise ValueError("COOL flag doesn't break down by run here: %s %s %s %s" % (sincerun, sincelb, untilrun, untillb))
                if ((sincelb == 0 and untillb == 1)
                    or (sincelb == untillb)
                    or (sincelb == 0xFFFFFFFF and untillb == -1)):
                    # Invalid LB, ignore
                    continue
                if sincerun in run_list:
                    if obj.Code not in codeMap: continue
                    runstr = repr(int(sincerun))
                    if untillb == THIRTYTWOMASK: untillb=-1
                    if untillb > SIMAX: untillb = -1
                    if newfmt:
                        channel = folder.channelName(obj.channel)
                        if runstr not in rv:
                            rv[runstr] = {}
                        if channel not in rv[runstr]:
                            rv[runstr][channel] = []
                        rv[runstr][channel].append((sincelb, untillb, codeMap[obj.Code]))
                    else:
                        if runstr not in rv:
                            rv[runstr] = []
                        rv[runstr].append((sincelb, untillb, codeMap[obj.Code]))
                    if sincelb > 0xFFFF or untillb > 0xFFFF:
                        print(flag, folder_in, tag, sincerun, sincelb, untillb)
            if newfmt:
                for rundict in list(rv.values()):
                    for lbblist in list(rundict.values()):
                        lbblist.sort()
            else:
                for lbblist in list(rv.values()):
                    lbblist.sort()

    #db.closeDatabase()
    #if folder_in == 'SHIFTOFL': print rv
    return rv

def iov_intersector(runs, objiovs):
    for objiov in objiovs:
        for run in runs:
            rviov = objiov.intersect_run(run)
            if rviov.until > rviov.since:
                yield rviov

def get_defects_lb(run_spec, defects=None, tag='HEAD', with_time=False, nonpresent=False,
                   db='Production', with_full_info=False, ignore=[]):
    """
Usage: get_defects_lb(run_spec, defects, tag, with_time, nonpresent,
                      db, with_full_info, ignore)

Arguments:

    * run_spec: run specification (see below)
    * defects: defects.  This argument can be either a single defect
      (e.g. 'PIXB') or a list of defects (e.g. ['PIXB', 'PIX0']).
      (default: empty string, which gives all primary defects)
    * tag: COOL tag (default: HEAD)
    * with_time: return time of insertion of information to DB (default: False)
    * nonpresent: also return defects explicitly marked absent (default: False)
    * db: sets DB to access; e.g. for Run 1 defects, set to 'Run 1' (default: 'Production')
    * with_full_info: return defect comments & recoverable status (default: False)
    * ignore: ignore the defects specified in this list when computing virtual defects (default: empty list [])

Returns:

    * dictionary with keys being run numbers (as strings) and values being:
        * if the "defects" argument is a string, the values will be a list of
          tuples (startlb, endlb+1, status)
        * if the "defects" argument is a list of strings, the values will be
          dictionaries with keys being the flag names and values being a
          list of tuples (startlb, endlb+1, status).
        * the tuples will have values (startlb, endlb+1, status), and, if
          requested by appropriate arguments, any or all of the following,
          in this order: 
          (insertion time, comment, is_recoverable)
    * NOTE: Status is the string "Red" when data should not be used. Other
      strings are reserved values and should be considered usable. Generally
      you will not get any tuple returned if the lumiblocks are good, unless you have
      set nonpresent=True.
    """
    from DQDefects import DefectsDB
    db = DefectsDB(DEFECTSDB[db], tag=tag)
    if defects == '': defects = None
    #db = DefectsDB(COOL_CONNS['DEFECTS'], tag=tag)
    #db = DefectsDB('oracle://ATLAS_COOLWRITE;schema=ATLAS_COOLOFL_GLOBAL;dbname=CONDBR2', tag=tag)
    import time

    if defects is None:
        defects = db.defect_names
        newfmt = True
    elif type(defects) == str:
        defects = [defects]
        newfmt = False
    else:
        newfmt = True

    ignore = set(ignore)

    if len(defects) < 1:
        return {}
    
    atlasdqm_core._canonicalize_runspec(run_spec)

    rv = {}
    run_list = run_spec['run_list']
    if run_list == []:
        return rv
    lowrun = min(run_list)
    highrun = max(run_list)
    
    objects = db.retrieve(channels=defects, since=(lowrun << 32 | 1),
                          until=(highrun << 32 | THIRTYTWOMASK),
                          intersect=True, with_time=with_time,
                          nonpresent=nonpresent, ignore=ignore,
                          evaluate_full=with_full_info)
    objects = list(iov_intersector(run_list, objects))
    SIMAX=int(2**31-1)
    for obj in objects:
        #if invmetadata.get(int(obj.channelId()), '') not in defects:
        #if folder.channelName(obj.channel) not in defects:
        #    continue
        sincerun = obj.since.run
        sincelb = obj.since.lumi
        untilrun = obj.until.run
        untillb = obj.until.lumi
        if obj.since >= obj.until:
            continue
        if (sincerun != untilrun and 
            ((untilrun - sincerun != 1) or
             (untillb != 0))):
            print('In get_defects_lb:')
            print('Defect over more than one run!', _fromIOV(obj.since), _fromIOV(obj.until), obj.channel)
            print(obj, tag)
            #print invmetadata[obj.channelId()], obj.since >> 32, obj.since & (2**32-1), obj.until >> 32, obj.until & (2**32-1), codeMap[obj.payload()['Code']]
            if untilrun - sincerun == 1:
                if untillb == 0:
                    untilrun = sincerun
                    untillb = -1
                elif sincelb == 0xFFFFFFFF and untillb <= 1:
                    # invalid info
                    continue
                else:
                    print("COOL flag doesn't break down by run here: %s %s %s %s" % (sincerun, sincelb, untilrun, untillb))

                        #raise ValueError("COOL flag doesn't break down by run here: %s %s %s %s" % (sincerun, sincelb, untilrun, untillb))
        if ((sincelb == 0 and untillb == 1)
            or (sincelb == untillb)
            or (sincelb == 0xFFFFFFFF and untillb == -1)):
            # Invalid LB, ignore
            continue
        if sincerun in run_list:
            runstr = repr(int(sincerun))
            if untillb == THIRTYTWOMASK: untillb=-1
            if untillb > SIMAX: untillb = -1
            obj_to_append_l = [sincelb, untillb, 'Red' if obj.present else 'Green']
            if with_time:
                obj_to_append_l.append(time.mktime(obj.insertion_time.timetuple()))
            if with_full_info:
                obj_to_append_l.append(obj.comment)
                obj_to_append_l.append(obj.recoverable)
                obj_to_append_l.append(obj.user)
            obj_to_append = tuple(obj_to_append_l)
#            if with_time:
#                obj_to_append = (sincelb, untillb, 'Red' if obj.present else 'Green', time.mktime(obj.insertion_time.timetuple()))
#            else:
#                obj_to_append = (sincelb, untillb, 'Red' if obj.present else 'Green')
            if newfmt:
                if runstr not in rv:
                    rv[runstr] = {}
                channel = obj.channel
                if channel not in rv[runstr]:
                    rv[runstr][channel] = []
                rv[runstr][channel].append(obj_to_append)
            else:
                if runstr not in rv:
                    rv[runstr] = []
                rv[runstr].append(obj_to_append)
            if sincelb > 0xFFFF or untillb > 0xFFFF:
                print(obj.channel, tag, sincerun, sincelb, untillb)
    if newfmt:
        for rundict in list(rv.values()):
            for lbblist in list(rundict.values()):
                lbblist.sort()
    else:
        for lbblist in list(rv.values()):
            lbblist.sort()

    #db.closeDatabase()
    #if folder_in == 'SHIFTOFL': print rv
    return rv

def get_run_information(run_spec):
    """
Usage: get_run_information(run_spec)

Arguments:

    * run_spec: run specification (see below)
    
Returns:

    * dictionary with keys being run numbers (as strings) and values being
      tuples with values (in order)
        - Run type (string)
        - Filename tag (string)
        - Partition name (string)
        - Number of events passing Event Filter (integer)
        - Run start (seconds after 1970) (integer)
        - Run end (seconds after 1970, 0 if run ongoing) (integer)
        - Number of luminosity blocks (-1 if run ongoing) (string)
        - Data source (string)
        - Detector mask (string)
        - Recording enabled (integer)
    """
    atlasdqm_core._canonicalize_runspec(run_spec)

    with _get_db(RUNINFODB, True) as db:
    #db = dbSvc.openDatabase(RUNINFODB, True)
        folder = db.getFolder('/TDAQ/RunCtrl/EOR')

        run_list = run_spec['run_list']
        lowrun = min(run_list)
        highrun = max(run_list)

        rv = {}
        objects = folder.browseObjects(lowrun << 32,
                                       (highrun << 32) + THIRTYTWOMASK,
                                       cool.ChannelSelection(0))
        for obj in objects:
            sincerun = obj.since() >> 32
            untilrun = obj.until() >> 32
            #print obj.payload()
            if sincerun != untilrun:
                print('COOL flag over more than one run!', _fromIOV(obj.since()), _fromIOV(obj.until()), obj.channelId())
                #print invmetadata[obj.channelId()], obj.since() >> 32, obj.since() & (2**32-1), obj.until() >> 32, obj.until() & (2**32-1), codeMap[obj.payload()['Code']]
            payload = obj.payload()
            if sincerun in run_list:
                rv[repr(int(payload['RunNumber']))] = [payload['RunType'],
                                            payload['T0ProjectTag'],
                                                   '',
                                                   -1,
                                            int(payload['SORTime']/1000000000),
                                            int(payload['EORTime']/1000000000),
                                            str((obj.until() & THIRTYTWOMASK)-1),
                                            'no LHC',
                                            str(int(payload['DetectorMask'],16)),
                                            payload['RecordingEnabled'],
                                                   ]

        #db = dbSvc.openDatabase(RUNINFODB, True)
        # run 2 now uses SOR instead of SOR_Params ...
        folder = db.getFolder('/TDAQ/RunCtrl/SOR')
        count = folder.countObjects(lowrun << 32,
                                    (highrun << 32) + THIRTYTWOMASK,
                                    cool.ChannelSelection(0))
        if count != len(rv):
            objects = folder.browseObjects(lowrun << 32,
                                           (highrun << 32) + THIRTYTWOMASK,
                                           cool.ChannelSelection(0))
            for obj in objects:
                sincerun = int(obj.since() >> 32)
                if repr(sincerun) in rv:
                    continue
                elif sincerun in run_list:
                    payload = obj.payload()
                    rv[repr(int(payload['RunNumber']))] = [payload['RunType'],
                                                payload['T0ProjectTag'],
                                                       '',
                                                       -1,
                                                int(payload['SORTime']/1000000000),
                                                0,
                                                '-1',
                                                'no LHC',
                                                str(int(payload['DetectorMask'],16)),
                                                payload['RecordingEnabled'],
                                                       ]

        folder = db.getFolder('/TDAQ/RunCtrl/EventCounters')
        objects = folder.browseObjects(lowrun << 32,
                                       (highrun << 32) + THIRTYTWOMASK,
                                       cool.ChannelSelection(0))
        for obj in objects:
            payload = obj.payload()
            sincerun = str(obj.since() >> 32)
            if sincerun not in rv:
                continue
            rv[sincerun][2:4] = [payload['PartitionName'], int(payload['EFEvents'])]
            #if payload['PartitionName'] == 'ATLAS':
            #print payload
            #print int(obj.since() >> 32), payload['PartitionName'], payload['EFEvents']

    #db.closeDatabase()
    #print rv
    return rv

def get_run_lb_lengths(run_spec):
    """
Usage: get_run_lb_lengths(run_spec)

Arguments:

    * run_spec: run specification (see below)
    
Returns:

    * dictionary with keys being run numbers (as strings) and values being
      tuples with the lengths of the lumi blocks (as integers)
    """
    atlasdqm_core._canonicalize_runspec(run_spec)

    #db = dbSvc.openDatabase(TRIGDB, True)
    with _get_db(TRIGDB, True) as db:
        folder = db.getFolder('/TRIGGER/LUMI/LBLB')

        run_list = run_spec['run_list']
        lowrun = min(run_list)
        highrun = max(run_list)

        intrv = {}
        objects = folder.browseObjects(lowrun << 32,
                                       (highrun << 32) + THIRTYTWOMASK,
                                       cool.ChannelSelection(0))
        for obj in objects:
            sincerun = obj.since() >> 32
            if sincerun in run_list:
                #untilrun = obj.until() >> 32
                lb = obj.since() & THIRTYTWOMASK
                #if sincerun != untilrun:
                #    print 'LB info over more than one run!', _fromIOV(obj.since()), _fromIOV(obj.until()), obj.channelId()
                payload = obj.payload()
                intrv[repr(startrun)] = (lb, max((payload['EndTime'] - payload['StartTime'])//1000000000,1))
        rv = {}
        for key in intrv:
            rv[key] = [time for lb, time in sorted(intrv[key])]

    #db.closeDatabase()
    db.closeDatabase()
    return rv
                                    


def get_run_magfields(run_spec):
    """
Usage: get_run_magfields(run_spec)

Arguments:

    * run_spec: run specification (see below)
    
Returns:

    * dictionary with keys being run numbers (as strings) and values being
      tuples with values (in order)
        - Soleniod actual current (float)
        - Toroid actual current (float)
        - Solenoid set current (float)
        - Toroid set current (float)
    """
    atlasdqm_core._canonicalize_runspec(run_spec)

    rv = {}
    #db = dbSvc.openDatabase(RUNINFODB, True)
    with _get_db(RUNINFODB, True) as db:
        folder = db.getFolder('/TDAQ/RunCtrl/SOR')

        run_list = run_spec['run_list']
        lowrun = min(run_list)
        highrun = max(run_list)

        objects = folder.browseObjects(lowrun << 32,
                                       (highrun << 32) + THIRTYTWOMASK,
                                       cool.ChannelSelection(0))

        for obj in objects:
            sincerun = obj.since() >> 32
            untilrun = obj.until() >> 32
            #print obj.payload()
            #print invmetadata[obj.channelId()], obj.since() >> 32, obj.since() & (2**32-1), obj.until() >> 32, obj.until() & (2**32-1), codeMap[obj.payload()['Code']]
            payload = obj.payload()
            if sincerun in run_list:
                rv[repr(int(payload['RunNumber']))] = payload['SORTime']
    #db.closeDatabase()
           
    #db = dbSvc.openDatabase(DCSDB, True)
    with _get_db(DCSDB, True) as db:
        folder = db.getFolder('/EXT/DCS/MAGNETS/SENSORDATA')

        for run in sorted(rv, key=int):
            objects = folder.findObjects(rv[run],
                                         cool.ChannelSelection.all())
            fields = [-1.]*4
            for obj in objects:
                channel = obj.channelId()
                if 0 < channel < 5:
                    #print 'channel', channel, 'value', obj.payload()['value']
                    fields[magfieldmap[channel]] = obj.payload()['value']
            rv[run] = fields
        
    #db.closeDatabase()

    return rv

def _lumi_dq_helper(runlb, dqinfo):
    run, lb = runlb
    run = int(run)
    sel = dqinfo.get(repr(run), None)
    if sel is None:
        return True
    sel = sel['LUMI']
    for l, h, flag in sel:
        if h == -1: h = 0xFFFFFFFF
        if l <= lb < h and flag == 'Red':
            #print 'Red', run, lb
            return False
    return True

def _lumi_ready_helper(runlb, readyinfo):
    iov = (runlb[0] << 32) + runlb[1]
    for start, end in readyinfo:
        if start <= iov < end:
            return True
    return False

def get_run_beamluminfo(run_spec):
    """
Usage: get_run_beamluminfo(run_spec)

Arguments:

   * run_spec: run specification (see below)
    
Returns:

   * dictionary with keys being run numbers (as strings) and values being
     tuples with values (in order)
      * Max beam energy during run (float)
      * Stable beam flag enabled during run (boolean)
      * ATLAS ready flag enabled during run (boolean)
      * Total integrated luminosity (/nb) - online best estimate (float)
      * ATLAS ready luminosity (/nb) - online best estimate (float)
    """
    atlasdqm_core._canonicalize_runspec(run_spec)

    # Run 2 - disable
    #lumi_dq = get_dqmf_summary_flags_lb(run_spec, ['LUMI'], 'SHIFTOFL', 'HEAD')

    rundur = {}; lbinfo = {}
    with _get_db(TRIGDB, True) as trigdb, _get_db(RUNINFODB, True) as db:
    #trigdb = dbSvc.openDatabase(TRIGDB, True)
    #lblestonl = trigdb.getFolder('/TRIGGER/LUMI/LBLESTONL')
    #db = dbSvc.openDatabase(RUNINFODB, True)
        lblb = trigdb.getFolder('/TRIGGER/LUMI/LBLB')
        luminosity = db.getFolder('/TDAQ/OLC/LUMINOSITY')
        #lblestonl = trigdb.getFolder('/TRIGGER/LUMI/LBLESTONL')
        lblestonl = trigdb.getFolder('/TRIGGER/LUMI/OnlPrefLumi')
        ready = db.getFolder('/TDAQ/RunCtrl/DataTakingMode')

        run_list = run_spec['run_list']
        lowrun = min(run_list)
        highrun = max(run_list)

        objects = lblb.browseObjects(lowrun << 32,
                                       (highrun << 32) + THIRTYTWOMASK,
                                       cool.ChannelSelection(0))

        for obj in objects:
            sincerun = obj.since() >> 32
            untilrun = obj.until() >> 32
            payload = obj.payload()
            if sincerun in run_list:
                thislb = int(obj.since() & THIRTYTWOMASK)
                if sincerun not in lbinfo: lbinfo[sincerun] = {}
                lbinfo[sincerun][thislb] = [payload['StartTime'], payload['EndTime'], 0]

        if len(lbinfo) < 1:
            #trigdb.closeDatabase(); db.closeDatabase()
            return {}

        ## LBLESTONL
        objects = lblestonl.browseObjects(lowrun << 32,
                                          (highrun << 32) + THIRTYTWOMASK,
                                          cool.ChannelSelection(0))
        for obj in objects:
            sincerun = obj.since() >> 32
            if sincerun not in lbinfo: continue
            thislb = obj.since() & THIRTYTWOMASK
            # Run 2 - need replacement logic
            try:
            #    if _lumi_dq_helper((sincerun, thislb), lumi_dq):
                    lbinfo[sincerun][thislb][2] = obj.payload()['LBAvInstLumi']
            except:
                print(sincerun, thislb, 'is not in LBLB')

        # ready for physics
        readylist = []; readyrunset = set()
        objects = ready.browseObjects(lowrun << 32,
                                          (highrun << 32) + THIRTYTWOMASK,
                                          cool.ChannelSelection(0))

        for obj in objects:
            #print obj.since() >> 32, obj.since() & 0xFFFFFFFF, obj.until() >> 32, obj.until() & 0xFFFFFFFF, obj.payload()
            if obj.payload()['ReadyForPhysics'] == 1:
                readylist.append((obj.since(), obj.until()))
                sincerun = obj.since() >> 32; untilrun = obj.until() >> 32
                if (obj.until() & THIRTYTWOMASK) == 1: untilrun -= 1
                if untilrun != 2147483647:
                    for run in range(sincerun, untilrun + 1):
                        readyrunset.add(run)

        import time
        for run in lbinfo:
            lbs = sorted(lbinfo[run])
            # Gah. Run Query only looks at E at *beginning* of last LB, so misses
            # LHC recycling. Sneak this in as an extra variable.
            rundur[run] = [lbinfo[run][lbs[0]][0], lbinfo[run][lbs[-1]][1], 
                           lbinfo[run][lbs[-1]][0],
    #                       sum([x[2]*(x[1]-x[0])/1e12 for x in lbinfo[run].values()]),
    #                       sum([x[2]*(x[1]-x[0])/1e12 for lb, x in lbinfo[run].items() if _lumi_ready_helper((run, lb), readylist)])
                           0, 0
                           ]


        # /TDAQ/OLC/LUMINOSITY
        trl = sorted(rundur)
##         #print trl
##         objects = luminosity.browseObjects(rundur[trl[0]][0],
##                                           rundur[trl[-1]][1],
##                                           cool.ChannelSelection(0))

##         for obj in objects:
##             payload = obj.payload()
##             sincerun = payload['RunLB'] >> 32
##             if sincerun not in lbinfo: continue
##             thislb = payload['RunLB'] & THIRTYTWOMASK
##             try:
##                 if payload['LBAvInstLumPhys'] > 0 and _lumi_dq_helper((sincerun, thislb), lumi_dq):
##                     lbinfo[sincerun][thislb][2] = payload['LBAvInstLumPhys']
##             except Exception, e:
##                 print 'EXCEPTION', sincerun, thislb, `e`

        for run in rundur:
            rundur[run][3] = sum([x[2]*(x[1]-x[0])/1e12 for x in list(lbinfo[run].values())])
            rundur[run][4] = sum([x[2]*(x[1]-x[0])/1e12 for lb, x in list(lbinfo[run].items()) if _lumi_ready_helper((run, lb), readylist)])

        #for run in sorted(rundur):
        #    print run, 'start', time.ctime(rundur[run][0]/1e9), 'end', time.ctime(rundur[run][1]/1e9), 'lum', rundur[run][2]
        #for k in sorted(lbinfo[152875]):
        #    print k, lbinfo[152875][k][2]

    #db.closeDatabase()
    #trigdb.closeDatabase()
           
    with _get_db(DCSDB, True) as db:
        rv = {}
        for run in rundur:
            rv[repr(int(run))] = [0, 0, run in readyrunset, rundur[run][3], rundur[run][4]]
        # OK, we are going to cheat now, since FILLSTATE folder does not exist yet for Run 2
        if True:
            folder = db.getFolder('/LHC/DCS/FILLSTATE')
            for run in sorted(rundur, key=int):
                objects = folder.browseObjects(rundur[run][0], rundur[run][2],
                                               cool.ChannelSelection(1))
                wasstable = False
                maxe = 0
                for obj in objects:
                    payload = obj.payload()
                    if payload['StableBeams']: wasstable = True
                    beame_thislb = payload['BeamEnergyGeV']
                    if beame_thislb > maxe and beame_thislb < 7864 :
                        maxe = beame_thislb
                    #if run == 153599:
                        #print 153599, obj.since(), beame_thislb

                #if run == 153599: print '\n'.join([str(x) for x in lbinfo[run].items()])
                rv[repr(int(run))][0:2] = [maxe, wasstable]
                #if wasstable or True:
                #    print run, rundur[run][0], rundur[run][1], rv[`run`]
                #rv[run] = fields
        else:
            for run in rundur:
                rv[repr(int(run))][0:2] = [0, False]
    #db.closeDatabase()

    return rv

def get_events_in_streams(run_spec):
    """
Usage: get_events_in_streams(run_spec)

Arguments:

    * run_spec: run specification (see below)
    
Returns:

    * dictionary with keys being run numbers (as strings) and values being
      dictionaries with values (in order)
        - Run type (string)
    """

    from sqlalchemy import (create_engine, select, delete, update,
                        MetaData, Table, func, and_)
    from . import dbauth, atlasdqm_config

    rv = {}
    atlasdqm_core._canonicalize_runspec(run_spec)
    run_list = run_spec['run_list']
    if run_list == []:
        return rv
    lowrun = min(run_list)
    highrun = max(run_list)
    
    engine = create_engine(str(dbauth.get_authentication(atlasdqm_config.ATLAS_SFO_DB)))
    md = MetaData(engine)
    sfo_tz_file = Table('sfo_tz_file', md, autoload=True,
                           schema='ATLAS_SFO_T0')
    query = (select([sfo_tz_file.c.runnr, sfo_tz_file.c.streamtype,
                     sfo_tz_file.c.stream,
                     func.sum(sfo_tz_file.c.nrevents)],
                    and_(sfo_tz_file.c.runnr >= lowrun,
                         sfo_tz_file.c.runnr <= highrun,
                         sfo_tz_file.c.streamtype.in_(('physics', 'express'))
                    )
                    )
                    .group_by(sfo_tz_file.c.runnr)
                    .group_by(sfo_tz_file.c.stream)
             .group_by(sfo_tz_file.c.streamtype)
             )
    results = engine.execute(query)

    for row in results:
        runstr = repr(row[0])
        if runstr not in rv: rv[runstr] = {}
        rv[runstr]['%s_%s' % row[1:3]] = row[3]
    return rv

def get_end_of_calibration_period(run_spec):
    """
Usage: get_events_in_streams(run_spec)

Arguments:

    * run_spec: run specification (see below)
    
Returns:

    * dictionary with keys being run numbers (as strings) and values being
      pairs: the first value is the scheduled end of the Tier-0 calibration
      period in seconds past January 1 1970, the second is whether the loop
      has actually ended or not (1=ended, 0=still open)
    """

    from sqlalchemy import (create_engine, select, delete, update,
                        MetaData, Table, func, and_)
    from . import dbauth, atlasdqm_config

    rv = {}
    atlasdqm_core._canonicalize_runspec(run_spec)
    run_list = run_spec['run_list']
    if run_list == []:
        return rv
    lowrun = min(run_list)
    highrun = max(run_list)
    
    engine = create_engine(str(dbauth.get_authentication(atlasdqm_config.ATLAS_RUNNUMBER_DB)))
    md = MetaData(engine)
    nemop_task = Table('nemop_task', md, autoload=True,
                           schema='ATLAS_COOL_GLOBAL')
    query = (select([nemop_task.c.run, nemop_task.c.ttime,
                     nemop_task.c.state],
                    and_(nemop_task.c.ttype == 11,
                         nemop_task.c.run >= lowrun,
                         nemop_task.c.run <= highrun,
                         )
                    )
             )
    results = engine.execute(query)

    for row in results:
        runstr = repr(row[0])
        rv[runstr] = (row[1], row[2])
    return rv
        
        
def get_data_periods(run_spec):
    
    atlasdqm_core._canonicalize_runspec(run_spec)
    
    #from pyAMI.pyAMI import AMI
    #from xmlrpc.atlasdqm_config import AMI_CONF
    #amiclient = AMI(False)
    #amiclient.readConfig(AMI_CONF)

    from pyAMI.client import Client
    amiclient = Client('atlas')
    amiclient.config.read()

    rv = {str(run): [] for run in run_spec['run_list']}
    rl = ','.join(str(run) for run in run_spec['run_list'])

    result=amiclient.execute(['GetDataPeriodsForRun', '-runNumber=%s' % rl], format='dict_object')
    if not 'row' in result.rowsets[0]:
        store = []
    else:
        accum = {}
        result=result.get_rows()
        for row in result:
            run = row['runNumber']
            if run not in accum:
                accum[run] = []
            accum[run].append((int(row['periodLevel']),row['period'],row['project']))
        for run, v in accum.items():
            rv[run] = [_[1] for _ in sorted(v)]
    
    return rv

def get_reconstructed_fraction(run_spec):

    atlasdqm_core._canonicalize_runspec(run_spec)

    from . import han_extractor
    evt = get_events_in_streams(run_spec.copy())    
    
    # if processing ongoing, need "tmp" in the url
    run_spec_tmp = {}; 
    run_spec_tmp = run_spec.copy()
    run_spec_tmp['stream']= 'tmp_'+run_spec['stream']

    r1 = han_extractor.get_dqmf_all_results(run_spec,'Global/DataFlow/m_release_stage_lowStat')
    r2 = han_extractor.get_dqmf_all_results(run_spec_tmp,'Global/DataFlow/m_release_stage_lowStat')
    
    rv = {}
    for run in run_spec_tmp['run_list']:
        frac = 0.; flag='Undefined'
        if str(run) in r1:
            if evt[str(run)][run_spec['stream']] > 0 :
                frac = 100*r1[str(run)]['Average']/float(evt[str(run)][run_spec['stream']])                
                if frac > 20 :flag='Yellow'
                if frac > 99.9 : flag='Green'
        if str(run) in r2 and str(run) not in rv:
            if evt[str(run)][run_spec['stream']] > 0 :
                frac=100*r2[str(run)]['Average']/float(evt[str(run)][run_spec['stream']])
                if frac > 20 :flag='Yellow'
                if frac > 99.9 : flag='Green'
        rv[str(run)]=[frac,flag]
           
    return rv

def get_initial_signoff_time(run_spec, tag='HEAD'):
    """
Usage: get_initial_signoff_time(run_spec)

Arguments:

    * run_spec: run specification (see below)
    
Returns:

    * dictionary with keys being run numbers (as strings) and values being
    integers representing the time the GLOBAL_NOTCONSIDERED defect was unset,
    in seconds past January 1 1970.  If GLOBAL_NOTCONSIDERED is in fact set
    for the run, the run will not appear in the dictionary.
    """
    orv = get_defects_lb(run_spec, defects='GLOBAL_NOTCONSIDERED',
                             tag=tag, with_time=True, nonpresent=True)
    rv = {}
    for k, l in list(orv.items()):
        if l[0][2] == 'Red':
            continue
        rv[k] = int(l[0][3])
    return rv
    

if __name__ == '__main__':
    print('Starting')
    import time
    t0 = time.time()
    get_dqmf_summary_flags({'low_run': 90000,
                            'high_run': 92226 },
                           'FCALC')
    print(time.time()-t0)
##     t0 = time.time()
##     get_dqmf_summary_flags2({'low_run': 90000,
##                             'high_run': 92226 },
##                            'FCALC')
##     print time.time()-t0
    print('run information')
    info = get_run_information({'low_run': 152000,
                                }
                         )
##     for run in sorted(info, key=int):
##         if info[run][2] != 'ATLAS':
##             continue
##         print run, info[run]
##         pass
#    print 'mag fields'
#    maginfo = get_run_magfields({'low_run': 153000,}
#                         )
    print('beamluminfo')
    blinfo = get_run_beamluminfo({'low_run': 152000,}
                         )
#    for run in sorted(maginfo, key=int):
#        if info[run][2] != 'ATLAS':
#            continue
#        print run, maginfo[run]
#        pass

    for run in sorted(blinfo, key=int):
        if run not in info: continue
        print(run, info[run], blinfo[run])
